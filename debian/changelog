opengm (2.3.6+20160905-1) unstable; urgency=medium

  * New upstream snapshot. (Closes: #836413)

 -- Ghislain Antony Vaillant <ghisvail@gmail.com>  Tue, 06 Sep 2016 07:50:24 +0100

opengm (2.3.6+20160901-1) unstable; urgency=medium

  * New upstream snapshot. (Closes: #836216)

 -- Ghislain Antony Vaillant <ghisvail@gmail.com>  Fri, 02 Sep 2016 11:43:07 +0100

opengm (2.3.6+20160814-1) unstable; urgency=medium

  * New upstream snapshot. (Closes: #806379, #811878)
  * Update patch queue.
    - Drop disable-mem-logging-by-default.patch, applied upstream.
    - Drop fix-hdf5-detection.patch, fixed upstream.
  * Bump standards version to 3.9.8, no changes required.
  * Drop build of examples.
  * Skip usage of RPATH with CMake.
  * Assorted enhancements to rules file:
    - Add missing DEB_CXXFLAGS_MAINT_APPEND option.
    - Simplify and correct dh_auto_test override.
    - Remove superfluous comments.
    - Wrap, sort and format.

 -- Ghislain Antony Vaillant <ghisvail@gmail.com>  Tue, 30 Aug 2016 16:58:33 +0100

opengm (2.3.6+20160131-2) unstable; urgency=medium

  * Remove build dependency on vigra, not required.
  * d/rules: simplify dh_auto_test override.
  * d/rules: improve comments.
  * Add upstream patch fixing FBTFS errors on kfreebsd and hurd.
  * Install Python examples to the python-opengm-doc package.
  * d/u/metadata: fix formatting error.
  * d/gbp.conf: change packaging branch to debian/master (DEP-14).

 -- Ghislain Antony Vaillant <ghisvail@gmail.com>  Sat, 02 Apr 2016 12:09:26 +0100

opengm (2.3.6+20160131-1) unstable; urgency=medium

  * Team upload.

  [Ghislain Antony Vaillant]
  * Drop patch Fix-testsuite-execution-on-32-bit.patch,
    applied upstream.
  * Provide examples in doc package.
  * d/rules: build examples conditionally on nocheck.
  * d/gbp.conf: use upstream tag format.
  * d/rules: simplify dh_autotest override.
  * d/control: use secure VCS URIs.
  * Fix usage of embedded jquery in doc package.
  * d/rules: move dh_numpy call to dh_python2 override.
  * d/rules: exclude examples from compression.
  * Bump standards version to 3.9.7, no changes required.

  [Andreas Tille]
  * Avoid mixed quote signs in debian/upstream/metadata

 -- Ghislain Antony Vaillant <ghisvail@gmail.com>  Sun, 14 Feb 2016 14:27:29 +0000

opengm (2.3.6-2) unstable; urgency=medium

  [Andreas Tille]
  * Change source package priority to optional.

  [Ghislain Antony Vaillant]
  * gbp.conf: no patch numbering with gbp-pq.
  * Add patch fixing execution of testsuite on 32-bit arch.

 -- Ghislain Antony Vaillant <ghisvail@gmail.com>  Fri, 04 Dec 2015 09:49:43 +0000

opengm (2.3.6-1) unstable; urgency=low

  * Initial release. Closes: #801971

 -- Ghislain Antony Vaillant <ghisvail@gmail.com>  Tue, 20 Oct 2015 18:20:38 +0100
